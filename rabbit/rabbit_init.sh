#!bin/sh

(rabbitmqctl wait --timeout 10 $RABBITMQ_PID_FILE; \
rabbitmqctl add_vhost $RABBITMQ_HOST
rabbitmqctl add_user $RABBITMQ_ADMIN $RABBITMQ_ADMIN_PASSWORD 2>/dev/null; \
rabbitmqctl set_user_tags $RABBITMQ_ADMIN administrator 2>/dev/null; \
rabbitmqctl set_permissions -p $RABBITMQ_HOST $RABBITMQ_ADMIN  ".*" ".*" ".*" 2>/dev/null; \

rabbitmqctl add_user $RABBITMQ_PRODUCER $RABBITMQ_PRODUCER_PASSWORD 2>/dev/null; \
rabbitmqctl set_permissions -p $RABBITMQ_HOST $RABBITMQ_PRODUCER  "()" ".*" "()" 2>/dev/null; \

rabbitmqctl add_user $RABBITMQ_TG_CONSUMER $RABBITMQ_TG_CONSUMER_PASSWORD 2>/dev/null; \
rabbitmqctl set_permissions -p $RABBITMQ_HOST $RABBITMQ_TG_CONSUMER  "()" "()" ".*" 2>/dev/null; \

rabbitmqadmin -u $RABBITMQ_ADMIN -p $RABBITMQ_ADMIN_PASSWORD -V $RABBITMQ_HOST declare exchange name=$RABBITMQ_TG_EXCHANGE type=direct 2>/dev/null; \
rabbitmqadmin -u $RABBITMQ_ADMIN -p $RABBITMQ_ADMIN_PASSWORD -V $RABBITMQ_HOST declare queue name=$RABBITMQ_TG_QUEUE 2>/dev/null; \
rabbitmqadmin -u $RABBITMQ_ADMIN -p $RABBITMQ_ADMIN_PASSWORD -V $RABBITMQ_HOST declare binding source=$RABBITMQ_TG_EXCHANGE destination=$RABBITMQ_TG_QUEUE 2>/dev/null; \
) &
rabbitmq-server $@
