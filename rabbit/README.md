# RabbitMQ

## Описание [rabbit_init.sh](rabbit/rabbit_init.sh)
1. Ожидание запуска контейнера для его дальнейшей конфигурации
2. Создание виртуального хоста
3. Создание администратора
   - Администратор имеет все права на чтение, запись, создание всех сущностей, доступных в [RabbitMQ](https://www.rabbitmq.com/documentation.html)
4. Создание пользователя для публикации сообщений (используется на backend'е)
   - Имеет права только на публикацию
6. Создание пользователя для чтения сообщений (используется telegram ботом)
   - Имеет права только на чтение
8. Создание [точки обмена сообщениями](https://www.rabbitmq.com/tutorials/amqp-concepts.html)
9. Создание [очереди](https://www.rabbitmq.com/queues.html)
10. Связывание очереди и точки обмена

---
## Работа с RabbitMQ
### Добавление новых пользователей
1. Создать новую учетную запись `rabbitmqctl add_user {username} {password} 2>/dev/null;` 
2. Установить права `rabbitmqctl set_permissions -p {vhost} {username}  "{configure}" "{write}" "{read}" 2>/dev/null;`
### Добавление новых очередей
1. Создать новую очередь `rabbitmqadmin -u {admin} -p {admin password} -V {vhost} declare queue name={queue name}/dev/null;`
2. Связать новую очередь через binding (можно подключить к уже существующему exchenge'у) `rabbitmqadmin -u {admin} -p {admin password} -V {vhost} declare binding source={exchange} destination={queue name} 2>/dev/null;`

Может пригодиться при добавлении уведомлений в мобильном приложении, добавлении отедльного вида сообщений помимо рассылки и т.п.
### Работа с панелью администратора
* **Подключение**: {адрес сервера}:{rabbitmq admin port из переменных окружения}
* **Учетные данные**: логин и пароль администратора из переменных окружения

#### ! При изменении данной директории контейнер rabbitmq пересобирается

---
## Полезные ссылки

* **Документация**: https://www.rabbitmq.com/documentation.html
* **Цикл статей на Habr'е**: https://habr.com/ru/articles/488654/
