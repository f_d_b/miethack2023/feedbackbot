from .database import redis_engine
from datetime import datetime
from typing import Optional
import re


class User:
    def __init__(
        self,
        chat_id: str,
        access_token: str,
        refresh_token: str,
        username: str,
        is_admin: bool,
        last_message_date: Optional[datetime] = None
    ):

        if not last_message_date:
            last_message_date = datetime.now()

        self.chat_id = chat_id
        self.db_template = f'data:user:{chat_id}:'
        self.fsm_template = f'fsm:{chat_id}:{chat_id}:'
        self.access_token = access_token
        self.refresh_token = refresh_token
        self.username = username
        self.last_message_date = last_message_date
        self.is_admin = is_admin

    async def set_access_token(self, token: str):
        await redis_engine.set(self.db_template + 'access_token', token)
        self.access_token = token

    async def set_refresh_token(self, token: str):
        await redis_engine.set(self.db_template + 'refresh_token', token)
        self.refresh_token = token

    async def set_username(self, username: str):
        await redis_engine.set(self.db_template + 'username', username)
        self.username = username

    async def set_last_message_date(self, last_message_date: datetime):
        last_message_date_str = last_message_date.strftime('%Y-%m-%d %H:%M:%S')
        await redis_engine.set(self.db_template + 'last_message_date', last_message_date_str)
        self.last_message_date = last_message_date

    async def set_user_is_admin(self, is_admin: bool):
        await redis_engine.set(self.db_template + 'is_admin', int(is_admin))
        self.is_admin = is_admin

    async def save(self):
        """ Сохранить объект в Redis """
        await self.set_refresh_token(self.refresh_token)
        await self.set_access_token(self.access_token)
        await self.set_username(self.username)
        await self.set_last_message_date(self.last_message_date)
        await self.set_user_is_admin(self.is_admin)

        await redis_engine.set(f'data:chat_id:{self.username}:chat_id', self.chat_id)

    async def delete(self):
        """ Удаление данных о пользователе """
        for table in ['username', 'access_token', 'refresh_token', 'last_message_date', 'is_admin']:
            await redis_engine.delete(self.db_template + table)

    @staticmethod
    async def get(chat_id: str) -> 'User':
        """ Получить объект User по chat_id из Redis """
        db_template = f'data:user:{chat_id}:'
        access_token = (await redis_engine.get(db_template + 'access_token'))
        refresh_token = (await redis_engine.get(db_template + 'refresh_token'))
        username = (await redis_engine.get(db_template + 'username'))
        is_admin = (await redis_engine.get(db_template + 'is_admin'))

        last_message_date = await redis_engine.get(db_template + 'last_message_date')

        if access_token and username and last_message_date:
            is_admin = bool(int(is_admin.decode()))
            return User(
                chat_id=chat_id, 
                access_token=access_token.decode(),
                refresh_token=refresh_token.decode(),
                username=username.decode(),
                last_message_date=datetime.strptime(
                    last_message_date.decode(),
                    '%Y-%m-%d %H:%M:%S',
                ),
                is_admin=is_admin,
            )
        else:
            return None

    @staticmethod
    async def get_user_by_username(username: str) -> 'User':
        """ Получить объект User по username из Redis """
        chat_id = await redis_engine.get(f'data:chat_id:{username}:chat_id')
        if chat_id is None:
            return None
        return await User.get(chat_id.decode())
        
    @staticmethod
    async def get_all() -> list['User']:
        username_list = await redis_engine.keys('data:user:*:username')
        chat_id_list = [re.findall(r'\d+', temp_username.decode())[0] for temp_username in username_list]

        return [temp_user for temp_chat_id in chat_id_list if (temp_user := await User.get(temp_chat_id)) is not None]
