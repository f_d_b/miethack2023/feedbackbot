import aioredis
import os

redis_engine = aioredis.from_url(
    f"redis://{os.getenv('REDIS_HOST', 'localhost')}:{os.getenv('REDIS_PORT', 6379)}?password={os.getenv('REDIS_PASS', '1234')}"
)
