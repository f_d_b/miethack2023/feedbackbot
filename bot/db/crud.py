from datetime import datetime

from functools import wraps

from .models import User
from logic.logger import init_logger
from logic.messages_dataclasses import MESSAGES


logger = init_logger(__name__)


def user_data(func):
    @wraps(func)
    async def update_date(*args, **kwargs):
        # Получаем chat_id из объекта callback или message
        chat_id = args[1].from_user.id

        temp_user = await User.get(chat_id=chat_id)
        if temp_user is None:
            logger.error(f'Can\'t get user in redis with chat_id {chat_id}')
            await args[1].bot.send_message(
                text=MESSAGES['program_error'], chat_id=chat_id
            )
            return

        await temp_user.set_last_message_date(datetime.now())

        return await func(*args, **kwargs, user=temp_user)
    
    return update_date


async def set_redis_user(chat_id: str, access_token: str, refresh_token: str, username: str, is_admin: bool, last_message_date: datetime):
    new_user = User(
        chat_id=chat_id,
        access_token=access_token,
        refresh_token=refresh_token,
        username=username,
        is_admin=is_admin,
        last_message_date=last_message_date
    )
    try:
        await new_user.save()
    except Exception as ex:
        logger.error(f'Error when saving User to the Redis. Error message: {ex}')
