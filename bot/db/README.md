# Система хранения данных

## Струтура файлов
* [database.py](/bot/db/database.py) – создание подключения к Redis
* [models.py](/bot/db/models.py) – описание [пользовательской модели](#user)
* [crud.py](/bot/db/crud.py) – [взаимодействие](#crud) с пользовательской информации, хранящейся в Redis
* [enums.py](/bot/db/enums.py) – enum'ы, используемые в проекте

## User
Класс предоставляет функционал по чтению, изменению и удалению пользовательских данных из Redis

Данные располагаются в Redis по определенным шаблонам
* `chat_id` – telegram chat_id пользователя
* `access_token` – `data:user:{chat_id}:access_token`
* `refresh_token` – `data:user:{chat_id}:refresh_token`
* `username` – `data:user:{chat_id}:username`
* `last_message_date` – `data:user:{chat_id}:last_message_date`
* `state` и `data` `FSMContext'a` хранятся в `fsm:{chat_id}:{chat_id}:state` и `fsm:{chat_id}:{chat_id}:data` соответственно

## CRUD
Содержит функционал по работе с Redis:

* инициализация пользовательских данных при авторизации
* декоратор для добавления данных пользователя к handler'у и обновления поля последнего действия

---

## Полезные ссылки
* Официальная документация: https://redis.io/docs/about/
* Статья на Habr'е: https://habr.com/ru/companies/wunderfund/articles/685894/
* AORedis: https://aioredis.readthedocs.io/en/latest/getting-started/
