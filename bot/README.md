# Bot

В данной директории содержится весь код, относящийся к бизнес логике telegram bot'а

## Структура файлов
* [`DB`](/bot/db/) – хранилище
* [`Handlers`](/bot/handlers/) – handler'ы
* [`Logic`](/bot/logic/) – рассылка, взаимодействие с API
* [`main.py`](/bot/main.py) – запуск приложения
