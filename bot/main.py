import os

from aiogram import Bot, Dispatcher
from aiogram.utils import executor
from aiogram.utils.exceptions import ValidationError, NetworkError
from aiogram.contrib.fsm_storage.redis import RedisStorage2

from handlers.staff_commands import register_handlers as register_students_handlers
from handlers.base import register_handlers as register_base_handlers
from handlers.admin_commands import register_handlers as register_driver_handlers

from logic.mailing import MailMan
from logic.logger import init_logger
from logic.requests_to_api import Requester


logger = init_logger(__name__)


async def on_startup(_):
    logger.info('Starting bot')


if __name__ == "__main__":
    """
    Создается объект бота, диспатчера и хранилища данных.
    Регистрация обработчиков пользователесного ввода.
    Запуск long polling'а
    """
    # Создание бота
    try:
        bot_token = os.getenv('BOT_TOKEN', '6818202255:AAHKpbqKWmVxmuQMIV0K9HrEIk9yNTmNLxk')
        
        # Создание хранилища данных
        redis_host = os.getenv('REDIS_HOST', 'localhost')
        redis_port = os.getenv('REDIS_PORT', '6379')
        redis_pass = os.getenv('REDIS_PASS', '1234')

        storage = RedisStorage2(host=redis_host, port=int(redis_port), password=redis_pass)

        bot = Bot(token=bot_token)
        dp = Dispatcher(bot, storage=storage)

        # создание запросника
        requester = Requester(server=os.getenv('SERVER_URL', 'http://localhost:8000/api/v1'))

        # Регистрания handlers
        logger.info('Register handlers')
        register_students_handlers(requester=requester, dp=dp)
        register_base_handlers(requester=requester, dp=dp)
        register_driver_handlers(requester=requester, dp=dp)

        # # Создание рассылок
        mail_man = MailMan(bot=bot)
        mail_man()

        # Запуск поллинга
        executor.start_polling(dp, on_startup=on_startup)

        dp.storage.close()
    except KeyError:
        logger.error('No token provided')
    except ValidationError:
        logger.error('Invalid bot token')
    except NetworkError:
        logger.error('No internet connection')
