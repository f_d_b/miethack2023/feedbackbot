from aiogram import types
from aiogram.dispatcher import Dispatcher

from db.crud import User, user_data

from logic.logger import init_logger
from logic.messages_dataclasses import TelegramMessageDataclass, MESSAGES
from logic.requests_to_api import Requester

from .FSMforms import AdminMainMenu
from .admin_keyboards import main_menu_keyboard
from .base import BaseHandler

logger = init_logger(__name__)


async def main_menu(data: types.Message | types.CallbackQuery):
    """ Главное меню """
    if isinstance(data, types.Message):
        message = data
    else:
        message = data.message
    await message.answer(
        MESSAGES['main_menu'],
        parse_mode='Markdown',
        reply_markup=main_menu_keyboard()
    )
    await AdminMainMenu.main_state.set()


class AdminHandler(BaseHandler):
    @user_data
    async def request_feedback(self, message: types.Message, user):
        pass

    def __call__(self, dp: Dispatcher) -> None:
        """ Регистрация хэндлеров """
        dp.register_message_handler(
            self.request_feedback,
            lambda message: message.text == 'Запросить отзыв о себе',
            state=[AdminMainMenu.main_state]
        )


def register_handlers(requester: Requester, dp: Dispatcher) -> None:
    AdminHandler(requester)(dp)
