# Handlers
В данной директории содержится функционал по обработке пользовательских сообщений

## Структура файлов
* [`FSMforms.py`](/bot/handlers/FSMforms.py) – все необходимые состояния и формы
* [`base.py`](/bot/handlers/.py) – handler'ы общих команд (авторизация, start/, help/ и т.п.)
* [`keyboards.py`](/bot/handlers/keyboards.py) – общие клавиатуры, используемые как преподавателями, так и студентами
* [`driver_commands.py`](/bot/handlers/admin_commands.py) – handler'ы для обработки сообщений от преподавателей
* [`driver_keyboards.py`](/bot/handlers/admin_keyboards.py) – клавиатуры, используемые при работе преподавателей
* [`student_commands.py`](/bot/handlers/staff_commands.py) – handler'ы для обработки сообщений от учеников
* [`student_keyboards.py`](/bot/handlers/staff_keyboards.py) –  клавиатуры, используемые при работе учеников
