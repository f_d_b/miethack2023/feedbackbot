from aiogram.types import (
    ReplyKeyboardMarkup,
    KeyboardButton,
    InlineKeyboardButton,
    InlineKeyboardMarkup
)

from datetime import datetime


def back_keyboard() -> ReplyKeyboardMarkup:
    """ Клавиатура возвращения """
    back = KeyboardButton('Назад')
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(back)
    return keyboard
