from collections import defaultdict

from aiogram import types
from aiogram.utils.exceptions import MessageToDeleteNotFound, MessageNotModified
from aiogram.dispatcher import Dispatcher, FSMContext

from datetime import datetime, timedelta

from .FSMforms import StaffMainMenu
from .base import BaseHandler
from db.crud import User, user_data
from logic.logger import init_logger
from logic.messages_dataclasses import TelegramMessageDataclass, MESSAGES
from logic.requests_to_api import Requester

from .staff_keyboards import (
    main_menu_keyboard,
)

logger = init_logger(__name__)


async def main_menu(message: types.Message):
    """ Главное меню """
    await message.answer(
        MESSAGES['main_menu'],
        parse_mode='Markdown',
        reply_markup=main_menu_keyboard()
    )
    await StaffMainMenu.main_state.set()


class StaffHandler(BaseHandler):
    def __call__(self, dp: Dispatcher) -> None:
        """ Регистрация хэндлеров """
        pass


def register_handlers(requester: Requester, dp: Dispatcher) -> None:
    StaffHandler(requester)(dp)
