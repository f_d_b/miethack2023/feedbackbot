from aiogram.dispatcher.filters.state import StatesGroup, State


# общие
class AuthForm(StatesGroup):
    """ Форма регистрации """
    username = State()
    password = State()


# формы админстритора
class AdminMainMenu(StatesGroup):
    """ Главное меню администратора """
    main_state = State()


# формы обычного пользователя
class StaffMainMenu(StatesGroup):
    """ Главное меню """
    main_state = State()
