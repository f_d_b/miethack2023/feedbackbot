from aiogram.types import (
    ReplyKeyboardMarkup,
    KeyboardButton,
    InlineKeyboardMarkup,
    InlineKeyboardButton,
)


def main_menu_keyboard() -> ReplyKeyboardMarkup:
    """ Главная клавиатура """
    request_own_feedback = KeyboardButton('Запросить отзыв о себе')
    request_feedback_about_user_button = KeyboardButton('Запросить отзыв о сотруднике')
    # exit_button = KeyboardButton('Выйти из учетной записи')
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(request_own_feedback)
    keyboard.add(request_feedback_about_user_button)
    # keyboard.add(exit_button)
    return keyboard


def back_to_main_menu() -> InlineKeyboardMarkup:
    keyboard = InlineKeyboardMarkup(row_width=1)
    main_menu = InlineKeyboardButton(text='⬅ В главное меню', callback_data='main_menu')
    keyboard.add(main_menu)

    return keyboard
