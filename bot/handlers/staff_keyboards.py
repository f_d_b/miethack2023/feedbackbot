from aiogram.types import (
    ReplyKeyboardMarkup,
    KeyboardButton,
)


def main_menu_keyboard() -> ReplyKeyboardMarkup:
    """ Главная клавиатура """
    request_own_feedback = KeyboardButton('Запросить отзыв о себе')
    # exit_button = KeyboardButton('Выйти из учетной записи')
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(request_own_feedback)
    # keyboard.add(exit_button)
    return keyboard
