import re
from datetime import datetime

from aiogram import types
from aiogram.dispatcher import Dispatcher, FSMContext

from logic.logger import init_logger
from logic.messages_dataclasses import TelegramMessageDataclass, MESSAGES
from logic.requests_to_api import Requester

from db.crud import (
    User,
    user_data,
    set_redis_user
)

from .FSMforms import AuthForm, AdminMainMenu, StaffMainMenu

logger = init_logger(__name__)


class BaseHandler:

    def __init__(self, requester: Requester):
        self.requester = requester

    def __call__(self, dp: Dispatcher) -> None:
        raise NotImplemented()


class BaseCommandsHandler(BaseHandler):
    """ Класс хэндлеров базовых команд """
    async def start_command(self, message: types.Message) -> None:
        """
        Приветствие
        :param message: сообщение от пользователя
        """

        await message.answer(MESSAGES['username_message'], parse_mode='Markdown')
        await AuthForm.username.set()

    async def help_command(self, message: types.Message) -> None:
        await message.answer(MESSAGES['help_message'], parse_mode='Markdown')

        return

    @user_data
    async def menu_command(self, message: types.Message, user: User) -> None:
        from .admin_commands import main_menu as admin_main_menu
        from .staff_commands import main_menu as staff_main_menu

        if user.is_admin:
            await admin_main_menu(message)
        else:
            await staff_main_menu(message)
        return

    async def set_username(self, message: types.Message, state: FSMContext):
        """ Ввод username """
        async with state.proxy() as data:
            data['username'] = message.text
        await AuthForm.next()
        await message.answer(MESSAGES['password_message'], parse_mode='Markdown')

    async def set_password(self, message: types.Message, state: FSMContext):
        """ Ввод пароля """
        from .admin_commands import main_menu as admin_main_menu
        from .staff_commands import main_menu as staff_main_menu

        async with state.proxy() as data:
            data['password'] = message.text
            username = data['username']
            response_data = await self.requester.auth_user(data=data.as_dict())
        if response_data.is_error:
            logger.error((
                f'Can\'t authorize {message.from_user.id} with username: {data["username"]}\n'
                + response_data.error_message + f'. Status code: {response_data.status_code}\n'
            ))
            await message.answer(response_data.error_message, parse_mode='Markdown')
            await state.finish()
            return
        logger.info(f'{message.from_user.id} authorized with username: {data["username"]}')

        user_obj = await self.requester.get_me(response_data.result['access'])

        # Добавляем информацию о пользователе в Redis
        await set_redis_user(
            chat_id=message.from_user.id,
            refresh_token=response_data.result['refresh'],
            access_token=response_data.result['access'],
            username=data['username'],
            is_admin=user_obj.result['user']['is_staff'],
            last_message_date=datetime.now()
        )

        await message.answer(MESSAGES['authorized'], parse_mode='Markdown')
        await message.bot.delete_message(message.from_user.id, message.message_id)
        if user_obj.result['user']['is_staff']:
            await admin_main_menu(message)
        else:
            await staff_main_menu(message)
        return

    @user_data
    async def reply(self, message: types.Message, user: User):
        try:
            msg = message.reply_to_message.text
        except AttributeError:
            await message.answer('не умею читать сообщения')
            return

        if not msg.startswith('Новый запрос №'):
            await message.answer('на такое не отвечаю')
            return

        request_id = re.search(r'Новый запрос №\d+\b', msg).group()
        request_id = int(request_id[request_id.find("№") + 1:])

        feedback_id = re.search(r'Ваш ответ №\d+\b', msg).group()
        feedback_id = int(feedback_id[feedback_id.find("№") + 1:])

        await self.requester.send_feedback(feedback_id=feedback_id, text=message.text, auth_token=user.access_token)

        await message.answer('Ваш ответ успешно записан')


    def __call__(self, dp: Dispatcher) -> None:
        """ Регистрация хэндлеров """
        dp.register_message_handler(self.start_command, commands=['start'], state='*')
        dp.register_message_handler(self.help_command, commands=['help'], state='*')
        dp.register_message_handler(self.menu_command, commands=['menu'], state='*')
        dp.register_message_handler(self.set_username, state=AuthForm.username)
        dp.register_message_handler(self.set_password, state=AuthForm.password)
        dp.register_message_handler(self.reply, state='*')


def register_handlers(requester: Requester, dp: Dispatcher) -> None:
    BaseCommandsHandler(requester)(dp)
