import asyncio
import json
import os
import random

import aio_pika
from aiogram import Bot
from aiogram.utils.exceptions import ChatNotFound, BotBlocked, CantParseEntities
from aiormq.exceptions import AMQPConnectionError

from db.models import User
from .logger import init_logger

logger = init_logger(__name__)


class MailMan:
    def __init__(self, bot: Bot):
        self.bot = bot
        self.connection = None

    async def consume(self):
        counter = 0
        while True:
            try:
                self.connection = await aio_pika.connect_robust(
                    host='localhost',
                    port=17652,
                    login=os.getenv('RABBITMQ_TG_CONSUMER', 'bot_consumer'),
                    password=os.getenv('RABBITMQ_TG_CONSUMER_PASSWORD', '4321'),
                    virtualhost=os.getenv('RABBITMQ_HOST', 'rabbitmq')
                )
                logger.info('Connect queue')
                break
            except AMQPConnectionError as ex:
                logger.error(ex)
                counter += 1
                logger.warning(f'Retry №{counter}')
                await asyncio.sleep(5)

        async with self.connection:
            # Creating channel
            channel = await self.connection.channel()

            # Connecting queue
            queue = await channel.get_queue(os.getenv('RABBITMQ_TG_QUEUE', 'TG_MESSAGE_QUEUE'))

            async with queue.iterator() as queue_iter:
                async for message in queue_iter:
                    async with message.process():
                        await self.process_message(message.body.decode())

    async def process_message(self, data: str):
        try:
            message = json.loads(data)
            logger.debug(f'Get message: {message}')
            text = message['text']
            username = message['username']
            user = await User.get_user_by_username(username)
            if user is None:
                return
            chat_id = user.chat_id
            try:
                logger.info(f'Sending message to {chat_id}')
                await self.bot.send_message(chat_id=chat_id, text=text, parse_mode='Markdown')
            except ChatNotFound:
                logger.warning(f'There are no such chat {chat_id}')
            except BotBlocked:
                logger.warning(f'User {chat_id} blocked user')
            except CantParseEntities:
                logger.warning(f'CantParseEntities {text}')
            await asyncio.sleep(random.randint(1, 5))
        except json.decoder.JSONDecodeError:
            logger.warning(f'Can\'t decode message: {data}')
        except KeyError as ex:
            logger.warning(f'Can\'t send message. Key error: {ex}')

    def __call__(self, *args, **kwargs):
        loop = asyncio.get_event_loop()
        loop.create_task(self.consume())
