from dataclasses import dataclass


@dataclass
class TelegramMessageDataclass:
    is_error: bool = False
    error_message: str = ''
    result: str | list | dict | None = None
    status_code: int = 200
    server_message: str = ''


MESSAGES = {
    'unauthorized': '❌ *Неверный логин или пароль!*\nНажмите /start и попробуйте снова',
    'server_error': '❌ *Технические проблемы*\nПриносим свои извинения. Попробуйте позже',
}
