import aiohttp
import pytz
import jwt
from datetime import datetime
from dateutil import parser

from logic.logger import init_logger

from logic.messages_dataclasses import TelegramMessageDataclass, MESSAGES

logger = init_logger(__name__)


class Requester:

    def __init__(self, server) -> None:
        self.server = server
        self.auth_token = None

    async def auth_user(self, data: dict) -> TelegramMessageDataclass:
        """ Аутентификация пользователя на сайте """
        result = TelegramMessageDataclass()

        async with aiohttp.ClientSession() as session:
            async with session.post(
                self.server + '/token/',
                json=data,
            ) as response:
                result.status_code = response.status
                if response.status == 200:
                    result.result = await response.json()
                else:
                    result.is_error = True
                    result.error_message = MESSAGES['unauthorized']
        return result

    async def get_me(self, auth_token: str) -> TelegramMessageDataclass:
        """ Аутентификация пользователя на сайте """
        result = TelegramMessageDataclass()

        async with aiohttp.ClientSession() as session:
            async with session.get(
                self.server + '/profiles/get_me/',
                headers={'Authorization': f'Bearer {auth_token}'},
            ) as response:
                result.status_code = response.status
                if response.status == 200:
                    result.result = await response.json()
                else:
                    result.is_error = True
                    result.error_message = MESSAGES['unauthorized']

        return result

    async def send_feedback(self, feedback_id: int, text: str, auth_token: str) -> TelegramMessageDataclass:
        result = TelegramMessageDataclass()

        async with aiohttp.ClientSession() as session:
            async with session.patch(
                self.server + f'/feedbacks/{feedback_id}/',
                json={'text': text},
                headers={'Authorization': f'Bearer {auth_token}'},
            ) as response:
                result.status_code = response.status
                if response.status == 200:
                    result.result = await response.json()
                else:
                    result.is_error = True
                    result.error_message = MESSAGES['server_error']

        return result
