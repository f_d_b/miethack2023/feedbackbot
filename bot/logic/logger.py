import logging
import sys


def init_logger(name: str):
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)

    # настройка обработчика и форматировщика в соответствии с нашими нуждами
    file_handler = logging.FileHandler('logs/bot_logs.log', mode='a')
    std_handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s')

    # добавление форматировщика к обработчику
    file_handler.setFormatter(formatter)
    std_handler.setFormatter(formatter)
    # добавление обработчика к логгеру
    logger.addHandler(file_handler)
    logger.addHandler(std_handler)

    return logger
